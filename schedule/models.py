from django.db import models
import uuid
# Create your models here.
class Schedule(models.Model):
    id_aktifitas = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False)
    nama_aktifitas = models.CharField(max_length = 100, unique=True)
    deskripsi = models.CharField(max_length = 500)
    tempat = models.CharField(max_length = 100)
    status = models.CharField(max_length = 100)
    tanggal = models.DateTimeField(blank=True)
    created = models.DateTimeField(auto_now_add=True)