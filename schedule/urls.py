from django.urls import path
from . import views

app_name = 'schedule'

urlpatterns = [
    path('listAktifitas', views.listSchedule, name='listschedule'),
    path('add', views.addSchedulePage, name="addSchedulePage"),
    path('addAktifitas', views.addSchedule, name="addschedule"),
    path('update/(?P<id_aktifitas>\w+)', views.updateSchedulePage, name="updateSchedulePage"),
    path('updateAktifitas/(?P<id_aktifitas>\w+)', views.updateSchedule, name="updateschedule"),
    path('delete/(?P<id_aktifitas>\w+)', views.deleteSchedule, name="deleteschedule"),
    path('detail/(?P<id_aktifitas>\w+)', views.detailSchedule, name="detailschedule"),
    
]