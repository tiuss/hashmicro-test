from django.apps import AppConfig


class TesthashmicroConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'testhashmicro'
