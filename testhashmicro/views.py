from django.shortcuts import render,redirect

# Create your views here.
def home(request):
    return render(request, 'home.html')

def pencarianHasil(request):
    msg = ""
    if request.method == "POST":
        if len(request.POST["pertama"]) == 0:
            msg = "Hasil : 0% (0/0)"
        elif len(request.POST["kedua"]) == 0:
            if len(request.POST["pertama"]) != 0: 
                totalHuruf = len(request.POST["pertama"]) - request.POST["pertama"].count(' ')
                msg = "Hasil : 0% (0/"+str(totalHuruf)+")"
            else:
                msg = "Hasil : 0% (0/0)"
        else:
            listHasil = perhitungan(request.POST["pertama"],request.POST["kedua"])
            hurufSama = listHasil[0]
            totalHuruf = len(request.POST["pertama"]) - request.POST["pertama"].count(' ')
            persen = listHasil[1]
            msg = "Hasil : "+str(persen)+"% "+"("+str(hurufSama)+"/"+str(totalHuruf)+")"
    
    return render(request, 'testing.html',{"msg":msg})

def perhitungan(pertama,kedua):
    first = pertama
    second = kedua
    first.replace(" ", "")
    second.replace(" ", "")
    arrFirst = list(first)
    arrSecond = list(second)
    hurufSama = 0
    hasil = 0

    for x in arrFirst :
        if x in arrSecond :
            hurufSama+=1
    
    hasil = (hurufSama/len(arrFirst))*100
    
    return [hurufSama,hasil]

def quiz(request):
    msg = ""
    jawabanBenar = ["almond", "anggur", "apel", "aprikot", "alpukat", "ara", "asam", "atap", "arbei", "abiu"]
    total = 10
    if request.method == "POST":
        if len(request.POST["jawaban"]) == 0:
            msg = "Hasil : 0% (0/0)"
        else :
            jawaban = request.POST["jawaban"]
            listJawaban = jawaban.split(",")
            nilai = 0
            benar = 0
            for x in listJawaban:
                for y in jawabanBenar :
                    if x == y.lower() :
                        benar +=1
                        jawabanBenar.remove(y)
            nilai = (benar/total)*100
            msg = "Nilai : "+str(nilai)+"% "+"("+str(benar)+"/"+str(total)+")"
    return render (request, 'quiz.html',{"msg":msg})

def bmi(request):
    msg = ""
    if request.method == "POST":
        if len(request.POST["tinggi"]) == 0 :
            if len(request.POST["berat"]) == 0 :
                msg = "Silahkan isi kolom berat dan tinggi"
            elif len(request.POST["berat"]) != 0 :
                msg = "Silahkan isi kolom tinggi"
        elif len(request.POST["tinggi"]) != 0 :
            if len(request.POST["berat"]) == 0 :
                msg = "Silahkan isi kolom berat"
            else:
                hasil = float(request.POST["berat"])/(float(request.POST["tinggi"])*float(request.POST["tinggi"]))
                if hasil < 18.5 :
                    msg = "Hasil Perhitungan : "+str(hasil)+" (Kekurangan berat badan)"
                elif hasil >= 18.5 and hasil < 25 :
                    msg = "Hasil Perhitungan : "+str(hasil)+" (Normal (ideal))"
                elif hasil >= 25 and hasil < 30 :
                    msg = "Hasil Perhitungan : "+str(hasil)+" (Kelebihan berat badan)"
                elif hasil > 30 :
                    msg = "Hasil Perhitungan : "+str(hasil)+" (Kegemukan (Obesitas))"
    return render (request, "bmi.html", {"msg":msg})
