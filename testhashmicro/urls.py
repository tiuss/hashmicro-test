from django.urls import path
from . import views

app_name = 'testhashmicro'

urlpatterns = [
    path('', views.home, name='home'),
    path('hasil', views.pencarianHasil, name="pencarianHasil"),
    path('bmi', views.bmi, name="bmi"),
    path('games', views.quiz, name="games"),
]